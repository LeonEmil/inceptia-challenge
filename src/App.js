
import './sass/style.scss';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'

import Home from './components/Home'
import Login from './components/Login'
import NotFound from './components/NotFound'

function App() {

  return (
    <Router basename={process.env.PUBLIC_URL}>  
      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route path="/ingresar" element={<Login />} />
        <Route element={() => { return <NotFound /> }}></Route>
      </Routes>
    </Router>
  )
}

export default App;
